#include<stdio.h>

int main() {
	int n;

	printf("Entrez un nombre : ");
	scanf("%d", &n);
	while (n>0) {
		printf("%d\n", n % 10);
		n = n/10; // ou n /= 10
	}
	return 0;
}	
